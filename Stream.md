# Stream Settings

## Gaming PC

- Encoding
	- NVENC
	- Use CBR
	- Enable CBR padding
	- 15000 bitrate
	- 0 custom buffer
- Broadcast Settings
	- `rtmp://192.168.1.137/live`
- Video
  - Base Resolution: 1920x1080
  - Downscale: 1.50
  - Filter: Lanczos
  - FPS: 48
- Advanced
  - NVENC Preset: High Performance
  - Encoding Profile: main
  - Keyframe Interval: 0
  - Use CFR

## Streaming PC

- Encoding
	- x264
	- Use CBR
	- Enable CBR padding
	- 2250 bitrate
- Broadcast Settings
	- Twitch
- Video
  - Base Resolution: 1280x720
  - Downscale: None
  - Filter: Lanczos
  - FPS: 48
- Advanced
  - x264 Preset: medium
  - Encoding Profile: main
  - Keyframe Interval: 2
  - Use CFR


### Other Options

- [Use FFMPEG and UDP](https://helping-squad.com/obs-studio-send-an-udp-stream-to-a-second-pc-using-obs/)

 > To use rtmp you would need to run a rtmp server (for example nginx with rtmp module) on your linux machine. But what you can do right away is using the Advanced output of OBS-Studio, then Switch the recording type to custom ffmpeg output and then output type to output to url.
As the URL you can use udp://client.ip.address:1234
The client can then watch the stream in vlc by opening a network stream with the url: udp://@:1234
You probably need to change the container format to mpegts for everything to work. Instead of udp you can also use other protocols with ffmpeg. See this wiki entry for more info: https://trac.ffmpeg.org/wiki/StreamingGuide#Pointtopointstreaming 

> https://obsproject.com/forum/resources/obs-studio-send-an-udp-stream-to-a-second-pc-using-obs.455/