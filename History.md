# Gaming History

## Summary

Games played at a competitive level

- TF2
- Warsow
- Counter Strike Source
- Quake
- Unreal Tournament 2004

## History

### TF2

Top 3 Highlander in UGC Platinum for 10 Seasons

<a href="https://www.youtube.com/watch?v=LE3tS1g2CuA" target="_blank"><img src="http://img.youtube.com/vi/LE3tS1g2CuA/0.jpg" width="300"></a>
<a href="https://www.youtube.com/watch?v=2Wc4ALTkpZQ" target="_blank"><img src="http://img.youtube.com/vi/2Wc4ALTkpZQ/0.jpg" width="300"></a>

| Season |     Division |                 Team | Place |
|--------|:------------:|---------------------:|-------|
| 6      | UGC Platinum | Looking Handsome     | 1st   |
| 7      | UGC Platinum | Looking Handsome     | 1st   |
| 8      | UGC Platinum | Gun Runners          | Top 8 |
| 9      | UGC Platinum | The Syndicate        | 3rd   |
| 10     | UGC Platinum | Deathforce9          | 3rd   |
| 11     | UGC Platinum | Menace To Society    | 2nd   |
| 12     | UGC Platinum | Murderforce 9000     | 3rd   |
| 13     | UGC Platinum | Electric Temptations | 2nd   |
| 14     | UGC Platinum | Ginyu Force          | 3rd   |
| 15     | UGC Platinum | Hateful 8 + Decimate | 3rd   |
| 16     | UGC Platinum | Acoomuma             | 1st   |

### Warsow

Top CA Player

<a href="https://www.youtube.com/watch?v=pvKTx8dT16I" target="_blank"><img src="http://img.youtube.com/vi/pvKTx8dT16I/0.jpg" width="200"></a>
<a href="https://www.youtube.com/watch?v=NkK80yRjrgM" target="_blank"><img src="http://img.youtube.com/vi/NkK80yRjrgM/0.jpg" width="200"></a>
<a href="https://www.youtube.com/watch?v=SPs8E1fWoXI" target="_blank"><img src="http://img.youtube.com/vi/SPs8E1fWoXI/0.jpg" width="200"></a>

### CSS

CAL-M 2 Seasons

| Place | LAN                    | Date       |
|-------|------------------------|------------|
| 1st   | BoxHeads LAN           | Mar 2005   |
| 1st   | Waldoman/GRLAN         | Jan 2006   |
| 1st   | BoxHeads LAN           | April 2006 |
| 1st   | Waldoman XIII          | May 2006   |
| 1st   | IB LAN                 | Feb 2007   |
| 1st   | Parkview Lan           | Nov 2007   |
| 1st   | MadLan                 | Nov 2007   |
| 2nd   | CyGamZ 2.6k Tournament | Dec 2007   |
| 1st   | MPCON XX               | Mar 2008   |
| 1st   | Big Shot Gaming        | Apr 2008   |

### Unreal Tournament 2004

Member of Top 8 TDM team

| Place | Tournament | LAN               | Date     |
|-------|------------|-------------------|----------|
| 7th   | 1v1        | Dayton Lanfest IV | Mar 2005 |
| 1st   | 1v1        | Parkview Lan 7    | Mar 2007 |
| 1st   | 3v3        | IB Lan            | Feb 2007 |
