# Gaming Configs

My gaming configs for multiple games. Includes a PowerShell script that symlinks the files to their specified location. This allows me to version all of my configs from one central location.

Uses [GamingConfigLinker](https://github.com/denolfe/GamingConfigLinker) to link the files.

## Usage

- Add game config
- Add details in `config.json` at the root level
- Execute `link.sh` or `link.cmd` to link files

## Software/Hardware Setup

- Current Mouse: G703
  - DPI: 800
  - Windows Sens: 6/11

| Name  | Weight | Length | Width | Height | LOD    |
| ----- | ------ | ------ | ----- | ------ | ------ |
| G703  | 107    | 124    | 68    | 43     | 1-2 CD |
| G403  | 90     | 124    | 68    | 43     | 1-2 CD |
| G400s | 105    | 130    | 70    | 43     | 3-4 CD |
| G Pro | 83     | 116.6  | 62.15 | 38.2   | 1-2 CD |
| G303  | 87     | 115    | 65    | 37     | 1-2 CD |
| FK2   | 85     | 124    | 58    | 36     | 1-2 CD |

- Mousepad: Glorious XL Heavy
  - LxWxH: 15.7 x 17.7 x 0.25

### Game-Specific Sensitivity (inches/360)

| Game        | in/360  |
|------       |-------: |
| Overwatch   | 15.152  |
| TF2         | 6.6     |
| COD:AW      | 5.9, 14.5 ADS |
| CSGO        | 25.5    |
| Warsow      | 5.7     |
| Quake       | 8.5     |
| CSS         | 25.2    |
| COD4        | 13.4    |
| Dirty Bomb  | 11.0    |
| LawBreakers | 11.25   |
